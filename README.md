# graphql-express-react

Following the tutorial on Youtube, I was used Express.js for the back-end to run a server GraphQL & React.js for the front-end to visualize how to interact with data. And the data which gets from SpaceX's API, the format of data is JSON.

# Port

## Server

You can change to the port to match with your system in the variable `PORT` this file: `server.js`:

![img](./images/port_server.png)

# Running this Project

## Running completely this project

You can run all this project with a simple command line:

```
    npm run dev
```

There are some pictures about this project when you run a command line above:
<br/> <br/> ![img](./images/home.png)
<br/> <br/> ![img](./images/detail.png)

## Client React.js (Front-end)

You can change to the port to match with your system in file `client > package.json`:

```
...
    "scripts": {
        "start": "PORT=3030 react-scripts start",
    },
...
```

In this project, I was default the PORT 3030 for Client. You can change any PORT you want.

And then you should run `npm i` to install necessary packages to run this project.

## Server Express.js (Back-end)

This project must be run by command line. And after that I will present step-by-step which can run this project:

1. Firstly, you should run `npm i` to install necessary packages to run this project.

2. Secondly, you must be run the server Express to interact Graphiql. And this is a command line which can run the server Express:

```
    npm run server
```

-   And this is a result after run a command line above:
    <br/> <br/> ![img](./images/run_server.png)

3. Thirdly, you will open your Browser and access the URL: http://localhost:5000/graphql. And this is a result after you access the URL above:
   <br/> <br/>![img](./images/browser.png)

4. Finally, this is queries API for GraphQL to get data from SpaceX's API:

-   Get all launches in SpaceX:
    <br/> <br/>![img](./images/launches.png)

-   Get a specific launch with its id:
    <br/> <br/>![img](./images/launch.png)

-   Get all rockets in SpaceX:
    <br/> <br/>![img](./images/rockets.png)

-   Get a specific rocket with its id:
    <br/> <br/>![img](./images/rocket.png)
