"use strict";

function _asyncToGenerator(fn) {
    return function () {
        var gen = fn.apply(this, arguments);
        return new Promise(function (resolve, reject) {
            function step(key, arg) {
                try {
                    var info = gen[key](arg);
                    var value = info.value;
                } catch (error) {
                    reject(error);
                    return;
                }
                if (info.done) {
                    resolve(value);
                } else {
                    return Promise.resolve(value).then(
                        function (value) {
                            step("next", value);
                        },
                        function (err) {
                            step("throw", err);
                        },
                    );
                }
            }
            return step("next");
        });
    };
}

var axios = require("axios");

var _require = require("graphql"),
    GraphQLObjectType = _require.GraphQLObjectType,
    GraphQLInt = _require.GraphQLInt,
    GraphQLString = _require.GraphQLString,
    GraphQLBoolean = _require.GraphQLBoolean,
    GraphQLList = _require.GraphQLList,
    GraphQLSchema = _require.GraphQLSchema;

// Launch Type

var LaunchType = new GraphQLObjectType({
    name: "Launch",
    fields: function fields() {
        return {
            flight_number: { type: GraphQLInt },
            mission_name: { type: GraphQLString },
            launch_year: { type: GraphQLString },
            launch_date_local: { type: GraphQLString },
            launch_success: { type: GraphQLBoolean },
            rocket: { type: RocketType },
        };
    },
});

// Rocket Type
var RocketType = new GraphQLObjectType({
    name: "Rocket",
    fields: function fields() {
        return {
            rocket_id: { type: GraphQLString },
            rocket_name: { type: GraphQLString },
            rocket_type: { type: GraphQLString },
        };
    },
});

// Root Query
var RootQuery = new GraphQLObjectType({
    name: "RootQuery",
    fields: {
        launches: {
            type: new GraphQLList(LaunchType),
            resolve: (function () {
                var _ref = _asyncToGenerator(
                    /*#__PURE__*/ regeneratorRuntime.mark(function _callee(
                        parent,
                        args,
                    ) {
                        var result;
                        return regeneratorRuntime.wrap(
                            function _callee$(_context) {
                                while (1) {
                                    switch ((_context.prev = _context.next)) {
                                        case 0:
                                            _context.next = 2;
                                            return axios.get(
                                                "https://api.spacexdata.com/v3/launches/",
                                            );

                                        case 2:
                                            result = _context.sent;
                                            return _context.abrupt(
                                                "return",
                                                result.data,
                                            );

                                        case 4:
                                        case "end":
                                            return _context.stop();
                                    }
                                }
                            },
                            _callee,
                            this,
                        );
                    }),
                );

                function resolve(_x, _x2) {
                    return _ref.apply(this, arguments);
                }

                return resolve;
            })(),
        },
        launch: {
            type: LaunchType,
            args: {
                flight_number: { type: GraphQLInt },
            },
            resolve: (function () {
                var _ref2 = _asyncToGenerator(
                    /*#__PURE__*/ regeneratorRuntime.mark(function _callee2(
                        parent,
                        args,
                    ) {
                        var result;
                        return regeneratorRuntime.wrap(
                            function _callee2$(_context2) {
                                while (1) {
                                    switch ((_context2.prev = _context2.next)) {
                                        case 0:
                                            _context2.next = 2;
                                            return axios.get(
                                                "https://api.spacexdata.com/v3/launches/" +
                                                    args.flight_number,
                                            );

                                        case 2:
                                            result = _context2.sent;
                                            return _context2.abrupt(
                                                "return",
                                                result.data,
                                            );

                                        case 4:
                                        case "end":
                                            return _context2.stop();
                                    }
                                }
                            },
                            _callee2,
                            this,
                        );
                    }),
                );

                function resolve(_x3, _x4) {
                    return _ref2.apply(this, arguments);
                }

                return resolve;
            })(),
        },
        rockets: {
            type: new GraphQLList(RocketType),
            resolve: (function () {
                var _ref3 = _asyncToGenerator(
                    /*#__PURE__*/ regeneratorRuntime.mark(function _callee3(
                        parent,
                        args,
                    ) {
                        var result;
                        return regeneratorRuntime.wrap(
                            function _callee3$(_context3) {
                                while (1) {
                                    switch ((_context3.prev = _context3.next)) {
                                        case 0:
                                            _context3.next = 2;
                                            return axios.get(
                                                "https://api.spacexdata.com/v3/rockets/",
                                            );

                                        case 2:
                                            result = _context3.sent;
                                            return _context3.abrupt(
                                                "return",
                                                result.data,
                                            );

                                        case 4:
                                        case "end":
                                            return _context3.stop();
                                    }
                                }
                            },
                            _callee3,
                            this,
                        );
                    }),
                );

                function resolve(_x5, _x6) {
                    return _ref3.apply(this, arguments);
                }

                return resolve;
            })(),
        },
        rocket: {
            type: RocketType,
            args: {
                rocket_id: { type: GraphQLString },
            },
            resolve: (function () {
                var _ref4 = _asyncToGenerator(
                    /*#__PURE__*/ regeneratorRuntime.mark(function _callee4(
                        parent,
                        args,
                    ) {
                        var result;
                        return regeneratorRuntime.wrap(
                            function _callee4$(_context4) {
                                while (1) {
                                    switch ((_context4.prev = _context4.next)) {
                                        case 0:
                                            _context4.next = 2;
                                            return axios.get(
                                                "https://api.spacexdata.com/v3/rockets/" +
                                                    args.rocket_id,
                                            );

                                        case 2:
                                            result = _context4.sent;
                                            return _context4.abrupt(
                                                "return",
                                                result.data,
                                            );

                                        case 4:
                                        case "end":
                                            return _context4.stop();
                                    }
                                }
                            },
                            _callee4,
                            this,
                        );
                    }),
                );

                function resolve(_x7, _x8) {
                    return _ref4.apply(this, arguments);
                }

                return resolve;
            })(),
        },
    },
});

module.exports = new GraphQLSchema({
    query: RootQuery,
});
