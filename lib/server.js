"use strict";

var _require = require("express-graphql"),
    graphqlHTTP = _require.graphqlHTTP;

var cors = require("cors");
var express = require("express");
var app = express();

var schema = require("./schema");

app.use(cors());
app.use(
    "/graphql",
    graphqlHTTP({
        schema: schema,
        graphiql: true,
    }),
);

// Change PORT
var PORT = process.env.PORT || 5000;

app.listen(PORT, function () {
    return console.log("Server is running on port " + PORT);
});
