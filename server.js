const { graphqlHTTP } = require("express-graphql");
const cors = require("cors");
const express = require("express");
const app = express();

const schema = require("./schema");

app.use(cors());
app.use(
    "/graphql",
    graphqlHTTP({
        schema,
        graphiql: true,
    }),
);

// Change PORT
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
