import React from "react";
import { gql, useQuery } from "@apollo/client";
import { Link } from "react-router-dom";
import classNames from "classnames";
import Moment from "react-moment";

const LAUNCH_QUERY = gql`
  query LaunchQuery($flight_number: Int!) {
    launch(flight_number: $flight_number) {
      flight_number
      mission_name
      launch_date_local
      launch_success
      rocket {
        rocket_id
        rocket_name
        rocket_type
      }
    }
  }
`;

function Launch(props) {
  let { flight_number } = props.match.params;
  flight_number = parseInt(flight_number);

  const { loading, error, data } = useQuery(LAUNCH_QUERY, {
    variables: { flight_number },
  });

  if (loading) return <h1>Loading...</h1>;
  if (error) return <p>Error :(</p>;

  const {
    mission_name,
    launch_date_local,
    launch_success,
    rocket,
  } = data.launch;
  return (
    <div>
      <h1 className="display-4 my-3">
        <span className="text-dark">Mission:</span> {mission_name}
      </h1>
      <h4 className="mb-3">Launch Detail</h4>
      <ul className="list-group">
        <li className="list-group-item">Flight Number: {flight_number} </li>
        <li className="list-group-item">
          Launch Year:{" "}
          <Moment format="DD-MM-YYYY HH:mm">{launch_date_local}</Moment>{" "}
        </li>
        <li className="list-group-item">
          Launch Success:{" "}
          <span
            className={classNames({
              "text-success": launch_success,
              "text-danger": !launch_success,
            })}
          >
            {launch_success ? "Yes" : "No"}
          </span>{" "}
        </li>
      </ul>

      <h4 className="my-3">Rocket Detail</h4>
      <ul className="list-group">
        <li className="list-group-item">Rocket Id: {rocket.rocket_id} </li>
        <li className="list-group-item">Rocket Name: {rocket.rocket_name} </li>
        <li className="list-group-item">Rocket Type: {rocket.rocket_type} </li>
      </ul>
      <hr />
      <Link to="/" className="btn btn-secondary">
        Back
      </Link>
    </div>
  );
}

export default Launch;
